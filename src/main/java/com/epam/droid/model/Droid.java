package com.epam.droid.model;

public class Droid {
    private String name;
    private int heath;

    public int getHeath() {
        return heath;
    }

    public String getName() {
        return name;
    }

    public Droid(String name, int heath) {
        this.name = name;
        this.heath = heath;
    }

    public void setHeath(final int cure) {
        heath += cure;
    }

    @Override
    public String toString() {
        return name + ":" + heath;
    }


}
