package com.epam.droid.model;

import java.util.LinkedList;
import java.util.List;

public class CarryDroid extends Droid {
    List<String> ammunition;

    public CarryDroid(String name, int heath) {
        super(name, heath);
        ammunition = new LinkedList<>();
    }

    public void setAmmunition(List<String> weapons) {
        ammunition = weapons;
    }

    public List<String> getAmmunition() {
        return ammunition;
    }

}
