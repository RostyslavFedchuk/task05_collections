package com.epam.droid.model;

import com.epam.collections.model.MyPriorityQueue;

import java.util.Queue;

public class Ship<T extends Droid> {
    Queue<T> droids;

    public Ship() {
        droids = new MyPriorityQueue<>();
    }

    public void putDroids(Queue<? extends T> droids) {
        for (T droid : droids) {
            this.droids.add(droid);
        }
    }

    public void putDroid(T droid) {
        droids.add(droid);
    }

    public Queue<T> getDroids() {
        return droids;
    }

    @Override
    public String toString() {
        return droids.toString();
    }
}
