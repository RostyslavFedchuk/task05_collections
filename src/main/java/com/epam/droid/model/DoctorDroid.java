package com.epam.droid.model;

public class DoctorDroid<T extends Droid> extends Droid {

    private static final int CURE_HEALTH = 25;

    public DoctorDroid(String name, int heath) {
        super(name, heath);
    }

    public void cureSomeone(T friend) {
        friend.setHeath(CURE_HEALTH);
    }
}
