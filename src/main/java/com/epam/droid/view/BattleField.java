package com.epam.droid.view;

import com.epam.droid.model.CarryDroid;
import com.epam.droid.model.DoctorDroid;
import com.epam.droid.model.Droid;
import com.epam.collections.model.MyPriorityQueue;
import com.epam.droid.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;

public class BattleField {

    private static Logger logger = LogManager.getLogger(BattleField.class);

    public static void main(String[] args) {
        Ship<Droid> ship = new Ship<>();
        ship.putDroids(fillShip());
        logger.info(ship.getDroids());
    }

    public static Queue<Droid> fillShip() {
        Queue<Droid> droids = new MyPriorityQueue<>();
        droids.offer(new CarryDroid("Carry", 75));
        droids.offer(new DoctorDroid<CarryDroid>("Doctor", 100));
        droids.offer(new Droid("Mike", 21));
        droids.offer(new Droid("Karl", 17));
        droids.offer(new Droid("Baron", 19));
        droids.offer(new Droid("Aaron", 10));
        return droids;
    }
}
