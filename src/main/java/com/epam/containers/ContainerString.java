package com.epam.containers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ContainerString {
    private static Logger logger = LogManager.getLogger(ContainerString.class);

    private static final int INITIAL_SIZE = 10;

    private static final int SIZE = 100;

    private String[] stringArray;

    private ContainerString() {
        stringArray = new String[INITIAL_SIZE];
    }

    public String get(int index) {
        if (!Objects.isNull(stringArray[index])) {
            return stringArray[index];
        } else {
            return "-1";
        }
    }

    private void add(String string) {
        for (int i = 0; i < stringArray.length; i++) {
            if (Objects.isNull(stringArray[i])) {
                stringArray[i] = string;
                return;
            }
        }
        stringArray = createNewArray();
        add(string);
    }

    private String[] createNewArray() {
        String[] newArray = new String[stringArray.length + INITIAL_SIZE];
        System.arraycopy(stringArray, 0, newArray, 0, stringArray.length);
        return newArray;
    }

    @Override
    public String toString() {
        String printArray = "[";
        for (int i = 0; i < stringArray.length; i++) {
            if (!Objects.isNull(stringArray[i])) {
                if (i == stringArray.length - 1
                        || Objects.isNull(stringArray[i + 1])) {
                    printArray += stringArray[i] + "";
                } else {
                    printArray += stringArray[i] + ", ";
                }
            } else {
                break;
            }
        }
        printArray += "]";
        return printArray;
    }

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        String[] choice = {"ArrayList", "MyContainer"};
        for (String choose : choice) {
            long initialTime;
            long endTime;
            logger.info("We are using " + choose + " with Strings");
            if (choose.equals("ArrayList")) {
                initialTime = System.nanoTime();
                ContainerString container = new ContainerString();
                for (int i = 0; i < SIZE; i++) {
                    container.add("" + i);
                }
                endTime = System.nanoTime();
                logger.info("It takes " + (endTime - initialTime) + " nanoseconds to add 100 elements");
            } else {
                initialTime = System.nanoTime();
                List<String> listContainer = new ArrayList<>();
                for (int i = 0; i < SIZE; i++) {
                    listContainer.add("" + i);
                }
                endTime = System.nanoTime();
                logger.info("It takes " + (endTime - initialTime) + " nanoseconds to add 100 elements");
            }
        }
    }
}
