package com.epam.containers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class TwoString implements Comparable<TwoString> {
    private static Logger logger = LogManager.getLogger(TwoString.class);
    private String country;
    private String capital;

    TwoString(String[] pair) {
        country = pair[0];
        capital = pair[1];
    }

    private String getCountry() {
        return country;
    }

    private String getCapital() {
        return capital;
    }

    @Override
    public int compareTo(TwoString o) {
        return this.getCountry().compareTo(o.getCountry());
    }

    public static void main(String[] args) {
        List<TwoString> list = fillList();
        logger.info("List before: " + list);
        Collections.sort(list);
        logger.info("List after sorting by country: " + list);
        list.sort(Comparator.comparing(TwoString::getCapital));
        logger.info("List after sorting by capital: " + list);
        logger.info("Index of Ukraine is "
                + Collections.binarySearch(list, new TwoString(new String[]{"Ukraine", "Kiev"})));
    }

    private static List<TwoString> fillList() {
        List<TwoString> list = new ArrayList<>();
        list.add(new TwoString(generatePair()));
        list.add(new TwoString(generatePair()));
        list.add(new TwoString(generatePair()));
        list.add(new TwoString(generatePair()));
        return list;
    }

    private static String[] generatePair() {
        String[] pair = new String[2];
        String[] capital = {"Lisbon", "Madrid", "Paris", "Berlin", "Warsaw",
                "Kiev", "Moscow", "Prague", "Rome"};
        String[] country = {"Portugal", "Spain", "France", "Germany",
                "Poland", "Ukraine", "Russia", "Czech Republic", "Italy"};
        Random random = new Random();
        pair[0] = country[random.nextInt(country.length)];
        pair[1] = capital[random.nextInt(capital.length)];
        return pair;
    }

    @Override
    public String toString() {
        return "[" + country + " - " + capital + "]";
    }
}
