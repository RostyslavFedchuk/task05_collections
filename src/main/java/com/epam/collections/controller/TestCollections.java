package com.epam.collections.controller;

import com.epam.collections.model.MyDeque;
import com.epam.collections.model.MyPriorityQueue;
import com.epam.collections.model.MyTreeMap;
import com.epam.collections.model.Property;
import com.epam.droid.model.Droid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class TestCollections implements Controller {
    private static Logger logger = LogManager.getLogger(TestCollections.class);
    private static Properties properties;
    private static final Scanner SCANNER;
    private Map<String, String> menu;
    private Map<String, Command> menuOptions;
    private Queue<Droid> droids;
    private Deque<Integer> deque;
    private Map<Integer, String> treeMap;

    static {
        SCANNER = new Scanner(System.in);
        properties = Property.INSTANCE.getProperties();
    }

    public TestCollections() {
        menu = ControllerMenus.getMenu();
        menuOptions = new LinkedHashMap<>();
        fillMenu();
    }

    private void fillMenu() {
        menuOptions.put("1", this::addValue);
        menuOptions.put("2", this::removeValue);
        menuOptions.put("3", this::printCollection);
        menuOptions.put("4", this::clearCollection);
        menuOptions.put("Q", this::quit);
    }

    @Override
    public void testMyPriorityQueue(String option) {
        droids = new MyPriorityQueue<>();
        logger.info("MyPriorityQueue<Droids> was created!");
        doWorkWithCollection(option);
    }

    @Override
    public void testMyDeque(String option) {
        deque = new MyDeque<>();
        logger.info("MyDeque<Integer> was created!");
        doWorkWithCollection(option);
    }

    @Override
    public void testMyTreeMap(String option) {
        treeMap = new MyTreeMap<>((o1, o2) -> o2 - o1);
        logger.info("MyTreeMap<Integer, String> was created!");
        doWorkWithCollection(option);
    }

    private void quit(String option) {
        String collection = properties.getProperty(option);
        logger.info("Stropped working with " + collection);
    }

    private void printMenu() {
        StringBuilder print = new StringBuilder("-----------Menu-----------\n");
        for (Map.Entry<String, String> entry : menu.entrySet()) {
            print.append(entry.getKey()).append(" - ").append(entry.getValue()).append("\n");
        }
        logger.info(print.toString());
    }

    private void addValue(String option) {
        String choice = properties.getProperty(option);
        try {
            if (choice.equals("MyPriorityQueue")) {
                addQueueElement();
            } else if (choice.equals("MyDeque")) {
                addDequeElement();
            } else if (choice.equals("MyTreeMap")) {
                addTreeMapElement();
            } else {
                logger.info("There is no methods for this!");
            }
            logger.info("Your element was successfully added!");
        } catch (InputMismatchException e) {
            logger.info("Your element was not added!");
        }
    }

    private void addDequeElement() {
        logger.info("Type 1 - to addFirst or 0 - to addLast:");
        int choice = SCANNER.nextInt();
        boolean isAddFirst = choice == 1;
        logger.info("Enter an integer to add to the deque:");
        int value = SCANNER.nextInt();
        if (isAddFirst) {
            deque.addFirst(value);
        } else {
            deque.addLast(value);
        }
    }

    private void addQueueElement() {
        logger.info("Enter droid`s name[String] and health[Integer]:");
        droids.offer(new Droid(SCANNER.next(), SCANNER.nextInt()));
    }

    private void addTreeMapElement() {
        logger.info("Enter an Integer and a String by example "
                + "without brackets: (1-One):");
        SCANNER.nextLine();
        String[] putter = SCANNER.nextLine().split("-");
        treeMap.put(Integer.valueOf(putter[0]), putter[1]);
    }

    private void removeValue(String option) {
        String choice = properties.getProperty(option);
        try {
            if (choice.equals("MyPriorityQueue")) {
                removeQueueElement();
            } else if (choice.equals("MyDeque")) {
                removeDequeElement();
            } else if (choice.equals("MyTreeMap")) {
                removeTreeMapElement();
            } else {
                logger.info("There is no methods for this!");
            }
            logger.info("Your element was successfully removed!");
        } catch (NoSuchElementException e) {
            logger.info("Your element was not removed!");
        }
    }

    private void removeDequeElement() {
        logger.info("Type 1 - to removeFirst or 0 - to removeLast:");
        int choice = SCANNER.nextInt();
        boolean isRemoveFirst = choice == 1;
        if (isRemoveFirst) {
            deque.removeFirst();
        } else {
            deque.removeLast();
        }
    }

    private void removeQueueElement() {
        logger.info("Type 1 - to removeFirst:");
        int choice = SCANNER.nextInt();
        boolean isRemoveFirst = choice == 1;
        if (isRemoveFirst) {
            droids.remove();
        }
    }

    private void removeTreeMapElement() {
        logger.info("Enter an Object key to remove:");
        treeMap.remove(SCANNER.nextInt());
    }

    private void printCollection(String option) {
        String choice = properties.getProperty(option);
        logger.info("Your " + choice + ":");
        if (choice.equals("MyPriorityQueue")) {
            logger.info(droids);
        } else if (choice.equals("MyDeque")) {
            logger.info(deque);
        } else if (choice.equals("MyTreeMap")) {
            logger.info(treeMap);
        } else {
            logger.info("There is no methods for this!");
        }
    }

    private void clearCollection(String option) {
        String choice = properties.getProperty(option);
        if (choice.equals("MyPriorityQueue")) {
            droids.clear();
        } else if (choice.equals("MyDeque")) {
            deque.clear();
        } else if (choice.equals("MyTreeMap")) {
            treeMap.clear();
        } else {
            logger.info("There is no methods for this!");
        }
        logger.info("Your " + choice + " was successfully cleared!");
    }

    private void doWorkWithCollection(String collectionIndex) {
        String option;
        do {
            printMenu();
            logger.info("Choose one option: ");
            option = SCANNER.next().toUpperCase();
            if (menuOptions.containsKey(option)) {
                menuOptions.get(option).execute(collectionIndex);
            } else {
                logger.warn("Incorrect option input! Try again.");
            }
        } while (!option.equals("Q"));
    }

}