package com.epam.collections.controller;

import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerMenus {

    static private Map<String, String> menu;

    static{
        menu = new LinkedHashMap<>();
        menu.put("1", "Add or put value");
        menu.put("2", "Remove value");
        menu.put("3", "Print collection");
        menu.put("4", "Clear collection");
        menu.put("Q", "Quit");
    }

    static public Map<String, String> getMenu() {
        return menu;
    }
}
