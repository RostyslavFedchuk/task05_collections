package com.epam.collections.controller;

public interface Controller {
    void testMyPriorityQueue(String option);

    void testMyDeque(String option);

    void testMyTreeMap(String option);
}
