package com.epam.collections.controller;

@FunctionalInterface
public interface Command<T> {
    void execute(String index);
}
