package com.epam.collections.view;

import com.epam.collections.controller.Controller;
import com.epam.collections.controller.TestCollections;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private Controller controller;

    private static Logger logger = LogManager.getLogger(Menu.class);

    private Map<String, String> menu;
    private Map<String, Printable> menuOptions;

    public Menu() {
        controller = new TestCollections();
        menu = new LinkedHashMap<>();
        menuOptions = new LinkedHashMap<>();
        fillMenu();
    }

    private void fillMenu() {
        menu.put("1", "Test MyPriorityQueue");
        menu.put("2", "Test MyDeque");
        menu.put("3", "Test MyTreeMap");
        menu.put("Q", "Quit");

        menuOptions.put("1", this::testMyPriorityQueue);
        menuOptions.put("2", this::testMyDeque);
        menuOptions.put("3", this::testMyTreeMap);
        menuOptions.put("Q", this::quit);
    }

    private void testMyPriorityQueue(String option) {
        controller.testMyPriorityQueue(option);
    }

    private void testMyDeque(String option) {
        controller.testMyDeque(option);
    }

    private void testMyTreeMap(String option) {
        controller.testMyTreeMap(option);
    }

    private void quit(String option) {
        logger.info("Good bye!");
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder("-----------Menu-----------\n");
        for (Map.Entry<String, String> entry : menu.entrySet()) {
            print.append(entry.getKey()).append(" - ").append(entry.getValue()).append("\n");
        }
        return print.toString();
    }

    public void showMenu() {
        Scanner scanner = new Scanner(System.in);
        String option;
        do {
            logger.info(this);
            logger.info("Choose one option: ");
            option = scanner.next().toUpperCase();
            if (menuOptions.containsKey(option)) {
                menuOptions.get(option).print(option);
            } else {
                logger.warn("Incorrect option input! Try again.");
            }
        } while (!option.equals("Q"));
    }
}
