package com.epam.collections.view;

@FunctionalInterface
public interface Printable {
    void print(String option);
}
