package com.epam.collections.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static java.util.Objects.*;

public class MyTreeMap<K, V> implements Map<K, V> {

    private static Logger logger = LogManager.getLogger(MyTreeMap.class);

    private EntryNode<K, V> root;

    private Comparator<K> comparator;

    private int size;

    public MyTreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    public MyTreeMap(Comparator<K> comparator, Map<? extends K, ? extends V> m) {
        this.comparator = comparator;
        putAll(m);
    }

    public class EntryNode<K, V> implements Entry<K, V> {
        K key;
        V value;
        EntryNode<K, V> left;
        EntryNode<K, V> right;

        EntryNode(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public EntryNode() {
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            this.value = value;
            return value;
        }

        @Override
        public String toString() {
            return key + "=" + value;
        }

        @Override
        public boolean equals(Object obj) {
            if(isNull(obj)){
                return false;
            }
            if(this == obj){
                return true;
            }
            if(obj instanceof EntryNode){
                EntryNode<K, V> object = (EntryNode<K, V>) obj;
                return getValue().equals(object.getValue())
                        && getKey().equals(object.getKey());
            } else {
                return false;
            }
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        String print = "[";
        if (nonNull(root)) {
            print += printTree(root);
        }
        print += "]";
        return print;
    }

    private String printTree(EntryNode<K, V> root) {
        String printer = "";
        if (isNull(root)) {
            return "";
        }
        printer += printTree(root.left) + root + ", " + printTree(root.right);
        return printer;
    }

    @Override
    public boolean isEmpty() {
        return nonNull(root);
    }

    @Override
    public boolean containsKey(Object key) {
        EntryNode<K, V> node = root;
        if (isNull(node) || isNull(key)) {
            return false;
        }
        while (nonNull(node)) {
            int compare = comparator.compare(node.getKey(), (K) key);
            if (compare > 0) {
                node = node.right;
            } else if (compare < 0) {
                node = node.left;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return isValue(root, value);
    }

    private boolean isValue(EntryNode<K, V> node, Object value) {
        boolean isContain;
        if (isNull(node)) {
            return false;
        }
        isContain = isValue(node.left, value);
        if (node.equals(new EntryNode<>(node.getKey(), (V) value))) {
            isContain = true;
        }
        if (isContain) {
            return true;
        }
        isContain = isValue(node.right, value);
        return isContain;
    }

    @Override
    public V get(Object key) {
        EntryNode<K, V> node = root;
        if (isEmpty()) {
            throw new NoSuchElementException("The Tree is empty");
        }
        if (isNull(key)) {
            throw new NoSuchElementException("The is no such element!!");
        }
        while (nonNull(node)) {
            int compare = comparator.compare(node.getKey(), (K) key);
            if (compare > 0) {
                node = node.right;
            } else if (compare < 0) {
                node = node.left;
            } else {
                return node.value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        EntryNode<K, V> putter = putUsingRecursion(root, key, value);
        if (isNull(root)) {
            root = putter;
        }
        return value;
    }

    private EntryNode<K, V> putUsingRecursion(EntryNode<K, V> node, K key, V value) {
        if (isNull(node)) {
            node = new EntryNode<>(key, value);
            size++;
            return node;
        }
        if (isNull(key)) {
            logger.info("The key of the map cannot be null!!!");
        }
        int compare = comparator.compare(node.getKey(), key);
        if (compare > 0) {
            node.right = putUsingRecursion(node.right, key, value);
        } else if (compare < 0) {
            node.left = putUsingRecursion(node.left, key, value);
        } else {
            node.value = value;
        }
        return node;
    }

    @Override
    public V remove(Object key) {
        EntryNode<K, V> node = root;
        V value = get(key);
        root = delete(node, key);
        return value;
    }

    private EntryNode<K, V> delete(EntryNode<K, V> node, Object key) {
        if (isNull(root)) {
            return null;
        }
        int compare = comparator.compare(node.getKey(), (K) key);
        if (compare < 0) {
            node.left = delete(node.left, key);
        } else if (compare > 0) {
            node.right = delete(node.right, key);
        } else {
            EntryNode<K, V> remover;
            if (isNull(node.right)) {
                node = node.left;
            } else if (isNull(node.left)) {
                node = node.right;
            } else {
                remover = node.left;
                if (nonNull(remover.right)) {
                    while (nonNull(remover.right.right)) {
                        remover = remover.right;
                    }
                    node.key = remover.right.getKey();
                    node.setValue(remover.right.getValue());
                    remover.right = remover.right.left;
                } else {
                    node.key = remover.getKey();
                    node.value = remover.getValue();
                    node.left = node.left.left;
                }
            }
        }
        return node;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if (m.size() == 0) {
            return;
        }
        for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        size = 0;
        root = null;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new LinkedHashSet<>();
        fillKeySet(root, keySet);
        return keySet;
    }

    private void fillKeySet(EntryNode<K, V> node, Set<K> keySet) {
        if (isNull(node)) {
            return;
        }
        fillKeySet(node.left, keySet);
        keySet.add(node.getKey());
        fillKeySet(node.right, keySet);
    }

    @Override
    public Collection<V> values() {
        Collection<V> values = new LinkedHashSet<>();
        fillValueCollection(root, values);
        return values;
    }

    private void fillValueCollection(EntryNode<K, V> node, Collection<V> values) {
        if (isNull(node)) {
            return;
        }
        fillValueCollection(node.left, values);
        values.add(node.getValue());
        fillValueCollection(node.right, values);
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet = new LinkedHashSet<>();
        fillEntrySet(root, entrySet);
        return entrySet;
    }

    private void fillEntrySet(EntryNode<K, V> node, Set<Entry<K, V>> entrySet) {
        if (isNull(node)) {
            return;
        }
        fillEntrySet(node.left, entrySet);
        entrySet.add(node);
        fillEntrySet(node.right, entrySet);
    }

}
