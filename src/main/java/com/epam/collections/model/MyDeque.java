package com.epam.collections.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class MyDeque<T> implements Deque<T> {

    private Logger logger = LogManager.getLogger(MyDeque.class);

    private List head;

    private List tail;

    private class List {
        private T value;
        List next;
        List prev;

        List(T value) {
            this.value = value;
        }
    }

    @Override
    public String toString() {
        StringBuilder resultDeque = new StringBuilder("[");
        List print = this.head;
        while (nonNull(print)) {
            if (isNull(print.next)) {
                resultDeque.append(print.value.toString());
            } else {
                resultDeque.append(print.value.toString()).append(", ");
            }
            print = print.next;
        }
        resultDeque.append("]");
        return resultDeque.toString();
    }

    @Override
    public void addFirst(T t) {
        if (isNull(head)) {
            this.head = new List(t);
            tail = head;
        } else {
            List first = new List(t);
            first.next = this.head;
            head.prev = first;
            head = first;
        }
    }

    @Override
    public void addLast(T t) {
        if (isNull(head)) {
            head = new List(t);
            tail = head;
        } else {
            tail.next = new List(t);
            tail.next.prev = tail;
            tail = tail.next;
        }
    }

    @Override
    public boolean offerFirst(T t) {
        if (isNull(head)) {
            this.head = new List(t);
            tail = head;
            return true;
        } else {
            List first = new List(t);
            first.next = this.head;
            this.head = first;
            List prev = tail;
            while (nonNull(prev.prev)) {
                prev = prev.prev;
            }
            prev.prev = first;
            return true;
        }
    }

    @Override
    public boolean offerLast(T t) {
        if (isNull(head)) {
            head = new List(t);
            tail = head;
            return true;
        }
        tail.next = new List(t);
        tail.next.prev = tail;
        tail = tail.next;
        return true;
    }

    @Override
    public T removeFirst() {
        if (isNull(head)) {
            return null;
        }
        List delete = this.head;
        this.head = head.next;
        if (nonNull(head)) {
            head.prev = null;
        }
        return delete.value;
    }

    @Override
    public T removeLast() {
        if (isNull(head)) {
            return null;
        }
        List delete = this.tail;
        this.tail = tail.prev;
        if (nonNull(tail)) {
            tail.next = null;
        }
        return delete.value;
    }

    @Override
    public T pollFirst() {
        return removeFirst();
    }

    @Override
    public T pollLast() {
        return removeLast();
    }

    @Override
    public T getFirst() {
        if (isNull(head)) {
            return (T) Integer.valueOf(-1);
        }
        return head.value;
    }

    public T get(int index) {
        if (isNull(head)) {
            return (T) Integer.valueOf(-1);
        }
        int indexGeneral = 0;
        List getter = this.head;
        while (nonNull(getter)) {
            if (indexGeneral == index) {
                return getter.value;
            } else {
                getter = getter.next;
                indexGeneral++;
            }
        }
        return (T) Integer.valueOf(-1);
    }

    @Override
    public T getLast() {
        if (isNull(head)) {
            return (T) Integer.valueOf(-1);
        }
        return tail.value;
    }

    @Override
    public T peekFirst() {
        return getFirst();
    }

    @Override
    public T peekLast() {
        return getLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (isNull(head)) {
            return false;
        }
        List delete = this.head;
        if (o == delete.value) {
            removeFirst();
            return true;
        }
        while (nonNull(delete.next)) {
            T valNext = delete.next.value;
            if (valNext == o) {
                if (delete.next.next != null) {
                    delete.next = delete.next.next;
                    delete.next.prev = delete;
                } else {
                    delete.next = null;
                    tail = tail.prev;
                }
                return true;
            } else {
                delete = delete.next;
            }
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (isNull(tail)) {
            return false;
        }
        List delete = tail;
        if (o == delete.value) {
            removeLast();
            return true;
        }
        while (nonNull(delete.prev)) {
            T valNext = delete.prev.value;
            if (valNext == o) {
                if (delete.prev.prev != null) {
                    delete.prev = delete.prev.prev;
                    delete.prev.next = delete;
                } else {
                    delete.prev = null;
                    head = head.next;
                }
                return true;
            } else {
                delete = delete.prev;
            }
        }
        return false;
    }

    public void deleteAll(T value) {
        if (isNull(head)) {
            logger.error("You can`t deal with nulls!!!");
            return;
        }
        List delete = this.head;
        while (nonNull(delete)) {
            if (value == delete.value) {
                removeFirst();
                delete = delete.next;
            } else {
                break;
            }
        }
        while (nonNull(delete.next)) {
            T valNext = delete.next.value;
            if (valNext == value) {
                if (delete.next.next != null) {
                    delete.next = delete.next.next;
                    delete.next.prev = delete;
                } else {
                    delete.next = null;
                    tail = tail.prev;
                }
            } else {
                delete = delete.next;
            }
        }
    }

    @Override
    public boolean add(T t) {
        return offerLast(t);
    }

    @Override
    public boolean offer(T t) {
        return offerLast(t);
    }

    @Override
    public T remove() {
        return removeFirst();
    }

    @Override
    public T poll() {
        return removeFirst();
    }

    @Override
    public T element() {
        return getFirst();
    }

    @Override
    public T peek() {
        return getFirst();
    }

    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        try {
            Iterator<T> listIterator = (Iterator<T>) c.iterator();
            while (listIterator.hasNext()) {
                if (!contains(listIterator.next())) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        try {
            Iterator<T> listIterator = (Iterator<T>) c.iterator();
            while (listIterator.hasNext()) {
                addLast(listIterator.next());
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        try {
            Iterator<T> iterator = (Iterator<T>) c.iterator();
            while (iterator.hasNext()) {
                deleteAll(iterator.next());
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        try {
            for (T next : this) {
                if (!c.contains(next)) {
                    deleteAll(next);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
    }

    @Override
    public boolean contains(Object o) {
        List container = head;
        while (nonNull(container)) {
            if (container.value == o) {
                return true;
            } else {
                container = container.next;
            }
        }
        return false;
    }

    @Override
    public int size() {
        if (isNull(head)) {
            return 0;
        }
        int size = 1;
        List sizer = head.next;
        while (nonNull(sizer)) {
            size++;
            sizer = sizer.next;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return isNull(head);
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            List node = head;

            @Override
            public boolean hasNext() {
                return nonNull(node);
            }

            @Override
            public T next() {
                List next = node;
                node = node.next;
                return next.value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size()];
        if (isNull(head)) {
            return null;
        }
        List converter = this.head;
        int i = 0;
        while (nonNull(converter)) {
            array[i] = converter.value;
            converter = converter.next;
            i++;
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        if (isNull(head)) {
            return null;
        }
        List converter = this.head;
        int i = 0;
        while (nonNull(converter)) {
            a[i] = (T1) converter.value;
            converter = converter.next;
            i++;
        }
        return a;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return new Iterator<T>() {
            List node = tail;

            @Override
            public boolean hasNext() {
                return nonNull(node);
            }

            @Override
            public T next() {
                List previous = node;
                node = node.prev;
                return previous.value;
            }
        };
    }
}
