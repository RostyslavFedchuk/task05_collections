package com.epam.collections.model;

import com.epam.droid.model.Droid;

import java.util.*;

import static java.util.Objects.*;

public class MyPriorityQueue<T extends Droid> extends AbstractQueue<T> {

    private Comparator<T> comparator;

    public MyPriorityQueue() {
        comparator = (o1, o2) -> {
            if (o1.getHeath() > o2.getHeath()) {
                return 1;
            } else if (o1.getHeath() == o2.getHeath()) {
                return o1.getName().compareTo(o2.getName());
            } else {
                return -1;
            }
        };
    }

    private List head;

    private class List {
        T value;
        List next;

        List(T value) {
            this.value = value;
        }
    }

    @Override
    public boolean offer(T o) {
        if (isNull(head)) {
            head = new List(o);
            return true;
        }
        List adder = this.head;
        if (comparator.compare(adder.value, o) >= 0) {
            return addFirst(o);
        }
        while (nonNull(adder.next)) {
            if (comparator.compare(adder.next.value, o) >= 0) {
                List paste = new List(o);
                paste.next = adder.next;
                adder.next = paste;
                return true;
            } else {
                adder = adder.next;
            }
        }
        return addLast(o);
    }

    private boolean addFirst(T o) {
        if (isNull(head)) {
            head = new List(o);
            return true;
        } else {
            List first = new List(o);
            first.next = this.head;
            this.head = first;
            return true;
        }
    }

    private boolean addLast(T o) {
        if (isNull(head)) {
            head = new List(o);
            return true;
        }
        List tail = this.head;
        while (nonNull(tail.next)) {
            tail = tail.next;
        }
        tail.next = new List(o);
        return true;
    }

    @Override
    public String toString() {
        StringBuilder resultQueue = new StringBuilder("[");
        List print = this.head;
        while (nonNull(print)) {
            if (isNull(print.next)) {
                resultQueue.append(print.value.toString());
            } else {
                resultQueue.append(print.value.toString()).append(", ");
            }
            print = print.next;
        }
        resultQueue.append("]");
        return resultQueue.toString();
    }

    @Override
    public T poll() {
        if (isNull(head)) {
            return null;
        }
        List poll = head;
        head = poll.next;
        return poll.value;
    }

    @Override
    public T peek() {
        if (isNull(head)) {
            return null;
        }
        return head.value;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private List node = head;

            @Override
            public boolean hasNext() {
                return nonNull(node);
            }

            @Override
            public T next() {
                List next = node;
                node = node.next;
                return next.value;
            }
        };
    }

    @Override
    public int size() {
        int size = 0;
        if (isNull(this.head)) {
            return 0;
        }
        List list = this.head;
        while (nonNull(list)) {
            size++;
            list = list.next;
        }
        return size;
    }

}
